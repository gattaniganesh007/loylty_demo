package com.gsquare.loyltydemo.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.gsquare.loyltydemo.R;
import com.gsquare.loyltydemo.model.Product;
import java.util.List;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ganesh on 10/05/17.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{

    private Context mContext;
    private List<Product> mResultList;

    public ProductAdapter(Context context, List<Product> resultList) {
        mContext = context;
        mResultList = resultList;
    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {

        View itemLayoutView =  LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_product, viewGroup, false);
        return new ProductAdapter.ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.mName.setText(mResultList.get(i).getName());

        // Load the cover pic of outlet
        Glide.with(mContext)
                .load(mResultList.get(i).getImageUrl())
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(viewHolder.mImage);

        viewHolder.mResultCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mResultList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @InjectView(R.id.product_image)
        ImageView mImage;
        @InjectView(R.id.product_name)
        TextView mName;
        @InjectView(R.id.result_card)
        CardView mResultCard;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}