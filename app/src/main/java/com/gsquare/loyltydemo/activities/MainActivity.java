package com.gsquare.loyltydemo.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import com.gsquare.loyltydemo.R;
import com.gsquare.loyltydemo.fragment.MainFragment;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends AppCompatActivity {

    Context mContext;
    @InjectView(R.id.tab_layout)
    TabLayout tabLayout;
    @InjectView(R.id.pager)
    ViewPager viewPager;
    @InjectView(R.id.toolbar)
    Toolbar mToolbar;
    @InjectView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    MainAdapter adapter;
    MainFragment tab1,tab2,tab3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mContext = MainActivity.this;
        ButterKnife.inject(this);

        setSupportActionBar(mToolbar);
        // Set title of Detail page
        collapsingToolbar.setTitle(getString(R.string.txt_fashion));
        collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.white));

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.txt_top_product)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.txt_best_seller)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.txt_new_arrival)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new MainAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public class MainAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public MainAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    tab1 = new MainFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("type", "TP");
                    tab1.setArguments(bundle);
                    return tab1;
                case 1:
                    tab2 = new MainFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("type", "BS");
                    tab2.setArguments(bundle1);
                    return tab2;
                case 2:
                    tab3 = new MainFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("type", "NA");
                    tab3.setArguments(bundle2);
                    return tab3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

}
