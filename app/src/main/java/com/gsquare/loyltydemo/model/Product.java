package com.gsquare.loyltydemo.model;

/**
 * Created by ganesh on 10/05/17.
 */

public class Product {

    private String Id;
    private String Name;
    private String ImageUrl;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }
}
