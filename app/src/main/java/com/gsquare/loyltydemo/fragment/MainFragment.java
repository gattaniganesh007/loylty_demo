package com.gsquare.loyltydemo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.gsquare.loyltydemo.R;
import com.gsquare.loyltydemo.adapter.ProductAdapter;
import com.gsquare.loyltydemo.model.Product;
import java.util.ArrayList;
import java.util.List;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainFragment extends Fragment {

    Context context;
    @InjectView(R.id.my_recycler_view)
    RecyclerView mRecyclerView;
    String type;

    ProductAdapter productAdapter;
    List<Product> productList = new ArrayList<Product>();

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, v);
        context = getActivity().getApplicationContext();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            type = bundle.getString("type", "TP");
        }

        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        productAdapter = new ProductAdapter(context, productList);
        mRecyclerView.setAdapter(productAdapter);

        // Set dummy data to listview
        fetchResult();

        return v;
    }

    public void fetchResult() {

        List<Product> tempProductList = new ArrayList<Product>();

        if(type.equals("TP")) {
            Product p = new Product();
            p.setId("1");
            p.setName("Top Product");
            p.setImageUrl("http://www.puneguides.in/demo/1.jpg");
            tempProductList.add(p);
        } else if(type.equals("BS")) {
            Product p1 = new Product();
            p1.setId("1");
            p1.setName("Best Seller");
            p1.setImageUrl("http://www.puneguides.in/demo/2.jpg");
            tempProductList.add(p1);
        } else {
            Product p2 = new Product();
            p2.setId("1");
            p2.setName("New Arrival");
            p2.setImageUrl("http://www.puneguides.in/demo/3.jpg");
            tempProductList.add(p2);
        }

        productList.addAll(tempProductList);
        productAdapter.notifyDataSetChanged();
    }
}
